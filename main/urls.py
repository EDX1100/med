from django.urls import path
from . import views

urlpatterns = [ 
    path('', views.index, name='index'),

    path('record', views.record, name='record'),
    path('doctorDetail/<int:pk>', views.doctorDetail.as_view(), name='doctorDetail'),

    path('paymentComplete', views.paymentComplete, name='paymentComplete'),

    path('talon_check', views.talon_check, name='talon_check'),
    path('export_ticket_xls/<int:pk>', views.export_ticket_xls, name='export_ticket_xls'),
    path('export_check_xls/<int:pk>', views.export_check_xls, name='export_check_xls'),
    
    path('profile', views.profile, name='profile'),

    path('services', views.services, name='services'),
]