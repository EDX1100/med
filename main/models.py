from django.db import models
from account.models import User

# Create your models here.

class Role(models.Model):
    nameRole                = models.CharField(max_length=20, verbose_name='Наименование роли')

    class Meta:
        verbose_name             = "Роль"
        verbose_name_plural      = "Роли - Админ"

    def __str__(self):
        return self.nameRole

class Auth(models.Model):
    login                   = models.CharField(max_length=100, verbose_name='Логин')
    password                = models.CharField(max_length=100, verbose_name='Пароль')

    class Meta:
        verbose_name             = "Аутентификация"
        verbose_name_plural      = "Аутентификация - Админ"

    def __str__(self):
        return self.login

class Employee(models.Model):
    surnameEmployee              = models.CharField(max_length=100, verbose_name='Фамилия сотрудника')
    nameEmployee                 = models.CharField(max_length=100, verbose_name='Имя сотрудника')
    middlenameEmployee           = models.CharField(max_length=100, verbose_name='Отчество сотрудника')
    email                        = models.CharField(max_length=100, verbose_name='Адрес электронной почты')
    phoneEmployee                = models.CharField(max_length=20, verbose_name='Номер телефона')

    fkRole                      = models.ForeignKey(Role, on_delete = models.DO_NOTHING, verbose_name = 'Роль', blank=True, null=True)
    fkAuth                      = models.ForeignKey(Auth, on_delete = models.DO_NOTHING, verbose_name = 'Данные авторизации', blank=True, null=True)

    class Meta:
        verbose_name             = "Сотрдник"
        verbose_name_plural      = "Сотрудники - Админ"

    def __str__(self):
        return self.surnameEmployee + ' ' + self.nameEmployee + ' ' + self.middlenameEmployee


class TimeTable(models.Model):
    dateTimeTable               = models.DateField(max_length=20, verbose_name='Дата приёма')
    time                        = models.TimeField(max_length=100, verbose_name='Время приёма')
    cabinet                     = models.IntegerField(verbose_name='Номер кабинета')
    status                      = models.BooleanField(verbose_name='Статус выбора',default=False)

    fkDepartment                = models.ForeignKey("Department", on_delete = models.DO_NOTHING, verbose_name = 'Отеделение')
    fkDoctor                    = models.ForeignKey("Doctor", on_delete = models.DO_NOTHING, verbose_name = 'Врач', default=1)

    class Meta:
        verbose_name            = "Расписание"
        verbose_name_plural     = "Расписания"

    def __str__(self):
        return str(self.dateTimeTable) + ' ' + str(self.time) + ' ' + str(self.cabinet)

    def get_absolute_url(self):
        return '/timeTable'


class Specialization(models.Model): 
    nameSpecialization          = models.CharField(max_length=50, verbose_name='Наименование специальности')

    class Meta:
        verbose_name            = "Специальность"
        verbose_name_plural     = "Специальности"

    def __str__(self):
        return self.nameSpecialization

    def get_absolute_url(self):
        return '/specialization'


class Service(models.Model):
    nameService                = models.CharField(max_length=100, verbose_name='Наименование услуги')
    description                = models.TextField(max_length=100, verbose_name='Описание услуги')
    structure                  = models.CharField(max_length=100, verbose_name='Состав услуги')
    price                      = models.CharField(max_length=20, verbose_name='Цена услуги')
    warranty                   = models.CharField(max_length=20, verbose_name='Гарантия')
    period                     = models.CharField(max_length=50, verbose_name='Срок действия')

    class Meta:
        verbose_name            = "Услуга"
        verbose_name_plural     = "Услуги"

    def __str__(self):
        return self.nameService

    def get_absolute_url(self):
        return '/service'


class Doctor(models.Model):
    surnameDoctor               = models.CharField(max_length=100, verbose_name='Фамилия доктора')
    nameDoctor                  = models.CharField(max_length=100, verbose_name='Имя доктора')
    middlenameDoctor            = models.CharField(max_length=100, verbose_name='Отчество доктора')
    descriptionDoctor           = models.TextField(max_length=200, verbose_name='Описание доктора')
    experience                  = models.CharField(max_length=100, verbose_name='Опыт работы')
    costOfAdmission             = models.IntegerField(verbose_name='Стоимость приёма')

    fkTimeTable                 = models.ManyToManyField(TimeTable,  verbose_name = 'Расписание', blank=True)
    fkSpecialization            = models.ManyToManyField(Specialization,  verbose_name = 'Специальность')
    fkService                   = models.ManyToManyField(Service,  verbose_name = 'Услуги')

    fkRole                      = models.ForeignKey(Role, on_delete = models.DO_NOTHING, verbose_name = 'Роль', blank=True, null=True)
    fkAuth                      = models.ForeignKey(Auth, on_delete = models.DO_NOTHING, verbose_name = 'Данные авторизации', blank=True, null=True)

    class Meta:
        verbose_name            = "Врач"
        verbose_name_plural     = "Врачи - Админ"

    def __str__(self):
        return self.surnameDoctor + ' ' + self.nameDoctor + ' ' + self.middlenameDoctor

    def get_absolute_url(self):
        return '/doctor'


class Pay(models.Model):
    statusPay                = models.CharField(max_length=10,verbose_name='Статус оплаты')
    typePay                  = models.CharField(max_length=100, verbose_name='Тип оплаты', blank=True, null=True)

    class Meta:
        verbose_name            = "Информация об оплате"
        verbose_name_plural     = "Информация об оплатах"

    def __str__(self):
        if self.typePay == None:
            return self.statusPay
        else:
            return self.statusPay + ' ' + str(self.typePay) 

    def get_absolute_url(self):
        return '/pay'



class Payer(models.Model):
    surnamePayer               = models.CharField(max_length=100, verbose_name='Фамилия плательщика')
    namePayer                  = models.CharField(max_length=100, verbose_name='Имя плательщика')
    middlenamePayer            = models.CharField(max_length=100, verbose_name='Отчество плательщика')
    phonePayer                 = models.CharField(max_length=20, verbose_name='Номер телефона')

    class Meta:
        verbose_name            = "Плательщик"
        verbose_name_plural     = "Плательщики"

    def __str__(self):
        return self.surnamePayer + ' ' + self.namePayer + ' ' + self.middlenamePayer

    def get_absolute_url(self):
        return '/payer'

class Check(models.Model):
    amount                       = models.IntegerField(verbose_name='Цена')

    dateCheck                  = models.DateField(max_length=50, verbose_name='Дата оплаты', auto_now_add=True, blank=True, null=True)
    timeCheck                  = models.TimeField(max_length=50, verbose_name='Время оплаты',auto_now_add=True, blank=True, null=True)

    fkPayer                    = models.ForeignKey(Payer, on_delete = models.DO_NOTHING, verbose_name = 'Плательщик')
    fkPay                      = models.ForeignKey(Pay, on_delete = models.DO_NOTHING, verbose_name = 'Оплата', default=3)

    class Meta:
        verbose_name            = "Чек"
        verbose_name_plural     = "Чеки"

    def __str__(self):
        return str(self.dateCheck) + '| Плательщик: ' + self.fkPayer.surnamePayer + ' ' + self.fkPayer.namePayer + ' ' + self.fkPayer.middlenamePayer + '| Цена: ' + str(self.amount)

    def get_absolute_url(self):
        return '/check'



class Ticket(models.Model):
    surnamePatient              = models.CharField(max_length=100, verbose_name='Фамилия пациента', default="Тест")
    namePatient                 = models.CharField(max_length=100, verbose_name='Имя пациента', default="Тест")
    middlenamePatient           = models.CharField(max_length=100, verbose_name='Отчество пациента', default="Тест")
    email                       = models.EmailField(max_length=100, verbose_name='Email', null=True, blank=True)
    phone                       = models.CharField(max_length=100, verbose_name='Номер телефона', null=True, blank=True)
    fkUser                      = models.ForeignKey(to='account.User', on_delete = models.DO_NOTHING, verbose_name='Пациент-пользователь', null=True, blank=True)
    fkTimeTable                 = models.ForeignKey(TimeTable, on_delete = models.DO_NOTHING, verbose_name = 'Расписание')
    fkDoctor                    = models.ForeignKey(Doctor, on_delete = models.DO_NOTHING, verbose_name = 'Врач')
    approval                    = models.BooleanField(verbose_name='Согласие', default=False)
    date_added                  = models.DateField( verbose_name='Дата создания',auto_now_add=True)

    class Meta:
        verbose_name            = "Талон на приём"
        verbose_name_plural     = "Талоны на приём"

    def __str__(self):
        return str(self.date_added)

    def get_absolute_url(self):
        return '/ticket'


class Direction(models.Model):
    nameDirection                = models.CharField(max_length=100, verbose_name='Наименование направления')
    description                  = models.TextField(max_length=100, verbose_name='Наименование диагноза')

    class Meta:
        verbose_name            = "Направление"
        verbose_name_plural     = "Направления"

    def __str__(self):
        return self.nameDirection


class Analize(models.Model):
    nameAnalize                = models.CharField(max_length=100, verbose_name='Наименование анализа')
    descriptionAnalize         = models.TextField(max_length=200, verbose_name='Описание анализа')

    class Meta:
        verbose_name            = "Анализ"
        verbose_name_plural     = "Анализы"

    def __str__(self):
        return self.nameAnalize


class Department(models.Model):
    nameDepartment               = models.CharField(max_length=50, verbose_name='Наименование отделения')
    addressDepartment            = models.CharField(max_length=50, verbose_name='Адрес отеделения')

    class Meta:
        verbose_name            = "Отеделние"
        verbose_name_plural     = "Отеделения"

    def __str__(self):
        return self.nameDepartment

    def get_absolute_url(self):
        return '/department'



