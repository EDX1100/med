from django import forms
from django.forms import ModelForm, TextInput, DateInput, Select, SelectMultiple, Textarea, CheckboxInput, NumberInput, EmailInput, FileInput
from main.models import *

class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = ['approval','fkTimeTable']

class PayerForm(ModelForm):
    class Meta:
        model = Payer
        fields = '__all__'

class TimeTableForm(ModelForm):
    class Meta:
        model = TimeTable
        fields = ['status']

class DoctorForm(ModelForm):
    class Meta:
        model = Doctor
        fields = ['surnameDoctor', 'nameDoctor','middlenameDoctor','costOfAdmission','fkTimeTable']

        widgets = {

            "fkTimeTable": Select(attrs={
                'class': 'form-control',
                'required': 'true',
            })
        }
