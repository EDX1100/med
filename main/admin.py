from django.contrib import admin
from .models import *   

# Register your models here.

# admin.site.register(Auth)
admin.site.register(Role)
# admin.site.register(Patient)
admin.site.register(Employee)
# admin.site.register(TimeTable)
# admin.site.register(Specialization)
# admin.site.register(Service) 
admin.site.register(Doctor) 
# admin.site.register(Pay) 
# admin.site.register(Department) 
# admin.site.register(Payer) 
# admin.site.register(Direction) 
# admin.site.register(Check) 
# admin.site.register(Analize) 
# admin.site.register(Ticket) 