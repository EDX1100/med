from json.decoder import JSONDecodeError
from django.http import HttpResponseRedirect
from django.http.response import JsonResponse
import xlwt
import json
from django.shortcuts import render, redirect, reverse,get_object_or_404
from django.db.models import Q
from django.views.generic import View, DeleteView, DetailView, UpdateView
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, request
from main.models import *
from main.forms import *

# Create your views here.

def export_ticket_xls(request, pk):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ticket.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Ticket Data')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Фамилия пациента', 'Имя пациента', 'Отчество пациента', 'Фамилия врача', 'Имя врача', 'Отчество врача',
    'Дата приёма', 'Время приёма','Номер кабинета' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    rows = Ticket.objects.filter(pk__in=[pk]).values_list('surnamePatient','namePatient','middlenamePatient', 'fkDoctor__surnameDoctor', 'fkDoctor__nameDoctor', 'fkDoctor__middlenameDoctor', 'fkTimeTable__dateTimeTable', 'fkTimeTable__time', 'fkTimeTable__cabinet' ,
    )
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)

    return response

def export_check_xls(request, pk):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="check.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Check Data')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ["Цена",'Фамилия плательщика', 'Имя плательщика', 'Отчество плательщика', 'Дата платежа', 'Время платежа', 'Статус оплаты', 'Тип оплаты' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    rows = Check.objects.filter(pk__in=[pk]).values_list('amount','fkPayer__surnamePayer', 'fkPayer__namePayer',
    'fkPayer__middlenamePayer', 'fkPayer__phonePayer','dateCheck','timeCheck','fkPay__statusPay','fkPay__typePay')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)

    return response

def index(request):
    return render(request, 'main/index.html')

def record(request):
    search_query = request.GET.get('search','')
    if search_query:
        elements = Doctor.objects.filter(Q(surnameDoctor__icontains=search_query) |
        Q(nameDoctor__icontains=search_query) |
        Q(middlenameDoctor__icontains=search_query) |
        Q(experience__icontains=search_query) |
        Q(costOfAdmission__icontains=search_query) |
        Q(fkSpecialization__nameSpecialization__icontains=search_query))
    else:
        elements = Doctor.objects.all()
    context = {'elements': elements}
    return render(request, 'main/record.html', context)

class doctorDetail(View):

    def get(self, request, pk):
        doctor = Doctor.objects.get(pk=pk)
        timetable = TimeTable.objects.filter(fkDoctor__in=[doctor])
        ticket_form = TicketForm()
        doctor_form = DoctorForm()
        return render(request, 'main/record_detail.html', context = {'ticket_form':ticket_form, 'doctor_form':doctor_form, 'data': doctor, 'timetable':timetable} )

@login_required(login_url='login')
def talon_check(request):
    talon = Ticket.objects.order_by('-id')[:1]
    check = Check.objects.order_by('-id')[:1]
    context = {'talon': talon,'check': check }
    return render(request, 'main/talon_check.html', context)


@login_required(login_url='login')
def paymentComplete(request):
    body = json.loads(request.body)

    if body['approvalPat'] == 'on':
        approvalPat = True
    else:
        approvalPat = False

    Ticket.objects.create(
        surnamePatient = body['surnamePat'],
        namePatient = body['namePat'],
        middlenamePatient = body['middlenamePat'],
        email = body['emailPat'],
        phone = body['phonePat'],
        fkUser = User.objects.get(id = request.user.id),
        fkTimeTable = TimeTable.objects.get(id = body['timeTable']),
        fkDoctor = Doctor.objects.get(id = body['doctor_id']),
        approval = approvalPat
    )

    TimeTable.objects.filter(id=body['timeTable']).update(status=True)

    Payer.objects.create(
        surnamePayer =  body['surnamePay'],
        namePayer = body['namePay'],
        middlenamePayer = body['middlenamePay'],
        phonePayer = body['phonePay']
    )

    Check.objects.create(
        fkPayer =  Payer.objects.get(id = Payer.objects.order_by('-id')[:1]),
        fkPay = Pay.objects.get(id = 1),
        amount = body['amount'],
    )
    print ('BODY:', body)
    return JsonResponse('Payment complete', safe=False)

@login_required(login_url='login')
def profile(request ):
    user_id = request.user.id
    last = Ticket.objects.filter(fkUser__in=[user_id]).order_by('-id')[:1]
    elements = Ticket.objects.filter(fkUser__in=[user_id]).order_by('-id')
    context = {'elements': elements, 'last':last }
    return render(request, 'main/profile.html', context)

def services(request):
    elements = Service.objects.all()
    context = {'elements': elements}
    return render(request, 'main/services.html', context)
