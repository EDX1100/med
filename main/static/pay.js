function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
const csrftoken = getCookie("csrftoken");

var total = "{{data.costOfAdmission}}";
var doctor_id = "{{data.id}}";

function completeOrder() {
  var url = "{% url 'paymentComplete' %}";

  fetch(url, {
    method: "POST",
    headers: {
      "content-type": "applications/json",
      "X-CSRFToken": csrftoken,
    },
    body: JSON.stringify({ doctor_id }),
  });
}

// Render the PayPal button into #paypal-button-container
paypal
  .Buttons({
    style: {
      color: "blue",
      shape: "rect",
      label: "pay",
      height: 40,
    },

    // Set up the transaction
    createOrder: function (data, actions) {
      return actions.order.create({
        purchase_units: [
          {
            amount: {
              value: total,
            },
          },
        ],
      });
    },

    // Finalize the transaction
    onApprove: function (data, actions) {
      return actions.order.capture().then(function (details) {
        // Show a success message to the buyer
        completeOrder();
        alert(
          "Транзакция совершена пользователем: " +
            details.payer.name.given_name +
            "!"
        );
      });
    },
  })
  .render("#paypal-button-container");
