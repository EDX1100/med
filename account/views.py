from django.shortcuts import render, redirect, reverse,get_object_or_404
from django.contrib.auth import authenticate, login, logout
from account.models import *
from account.forms import *

# Create your views here.

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('index')
        return render(request, 'registration/login.html')


def registerPage(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            form = UserForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                account = authenticate(username=username, password=password)
                login(request, account)
                return redirect ('/account/login')
            else:
                context = {'form': form}
        else:
            form = UserForm()
            context = {'form': form}
        return render(request, 'registration/register.html', context)

def logoutPage(request):
    logout(request)
    return redirect('index')