from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from main.models import *


# Create your models here.

class MyAccountManager(BaseUserManager):
    def create_user(self, username, surname, name, middlename, password=None):
        if not username:
            raise ValueError('Введите логин!')
        if not password:
            raise ValueError('Выберите пароль!')

        user = self.model(
                username=username,
                surname=surname,
                name=name,
                middlename=middlename,
            )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, surname, name,middlename):
        user = self.create_user(
                username=username,
                password=password,
                surname=surname,
                name=name,
                middlename=middlename,
            )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    username                   = models.CharField(max_length=50, unique=True, verbose_name='Логин')
    surname                    = models.CharField(max_length=100, verbose_name='Фамилия пациента', null=True)
    name                       = models.CharField(max_length=100, verbose_name='Имя пациента', null=True)
    middlename                 = models.CharField(max_length=100, verbose_name='Отчество пациента')
    birthDate                  = models.DateField(max_length=100, verbose_name='Дата рождения пациента', null=True)
    email                      = models.CharField(max_length=100, verbose_name='Адрес электронной почты', null=True)
    phone                      = models.CharField(max_length=20, verbose_name='Номер телеофна', null=True)
    approval                   = models.BooleanField( verbose_name='Согласие', default=False)
    fkDirection                = models.ManyToManyField(to='main.Direction', verbose_name = 'Направления', blank=True)
    date_joined                = models.DateTimeField(max_length=50, verbose_name='Дата регистраци', null=True, auto_now_add=True)
    last_login                 = models.DateTimeField(max_length=50, verbose_name='Дата последнего входа', null=True, auto_now=True)
    is_staff                   = models.BooleanField(default = False)
    is_admin                   = models.BooleanField(default = False)
    is_active                  = models.BooleanField(default = True)
    is_superuser               = models.BooleanField(default = False)

    fkAnalize                   = models.ManyToManyField(to='main.Analize', verbose_name = 'Анализы', blank=True)
    fkDirection                 = models.ManyToManyField(to='main.Direction', verbose_name = 'Направления', blank=True)
    
    
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['surname', 'name', 'middlename']

    objects = MyAccountManager()

    def __str__(self):
        return self.surname + ' ' + self.name + ' ' + self.middlename

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    class Meta:
        verbose_name            = "Пользователь"
        verbose_name_plural     = "Пользователи"

