from django import forms
from django.forms import ModelForm, TextInput, DateInput, Select, SelectMultiple, Textarea, CheckboxInput, NumberInput, EmailInput, FileInput
from account.models import User

class UserForm(ModelForm):
    class Meta: 
        model = User
        fields = ['username', 'password', 'surname', 'name', 'middlename', 'birthDate', 'email', 'phone', 'approval']

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user